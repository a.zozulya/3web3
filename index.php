<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['name'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}

// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
if (empty($_POST['birthyear'])) {
  print('Заполните год рождения.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['Sex'])) {
  print('Выберите пол.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['Limbs'])) {
  print('Выберите к-во конечностей.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['checkbox'])) {
  print('Подтверите согласие.<br/>');
  $errors = TRUE;
}

// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20977';
$pass = '2531605';
$db = new PDO('mysql:host=localhost;dbname=u20977', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
/*try {
  $stmt = $db->prepare("INSERT INTO application (name) SET name = ?");
  $stmt -> execute(array('fio'));
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}*/

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
$stmt = $db->prepare("INSERT INTO form (fio, birth,email,sex,limb,about,checkbox) VALUES (:fio, :birth,:email,:sex,:limb,:about,:checkbox)");
$stmt -> execute(array('fio'=>$_POST['name'], 'birth'=>$_POST['birthyear'],'email'=>$_POST['email'],'sex'=>$_POST['Sex'],'limb'=>$_POST['Limbs'],'about'=>$_POST['bio'],'checkbox'=>$_POST['checkbox']));


$stmt=$db->prepare("INSERT INTO all_abilities(ability_fly,ability_god,ability_telep,ability_telekines,ability_teleportion) VALUES(:fly,:god,:telepat,:telek,:telep)");
$mysuper=$_POST['superpower'];
for($i=0;$i<5;$i++)
{
  if($mysuper[$i]!=1)
  {
    $mysuper[$i]=0;
  }
}
$stmt->execute(array('fly'=>$mysuper[0],'god'=>$mysuper[1],'telepat'=>$mysuper[2],'telek'=>$mysuper[3],'telep'=>$mysuper[4]));


//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
